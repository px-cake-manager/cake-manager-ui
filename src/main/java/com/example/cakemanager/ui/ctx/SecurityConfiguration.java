package com.example.cakemanager.ui.ctx;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
class SecurityConfiguration {
	@Bean
	@ConditionalOnProperty(prefix = "spring.security.oauth2.client.registration", 
			name = "cakemanager.provider")
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		return http
				.authorizeRequests()
				.anyRequest().authenticated()
				.and()
				.oauth2Login()
				.and()
				.build();
	}
	
	@Bean
	@ConditionalOnProperty(prefix = "spring.security.oauth2.client.registration",
			name = "cakemanager.provider", havingValue = "none", matchIfMissing = true)
	public SecurityFilterChain fallbackFilterChain(HttpSecurity http) throws Exception {
		return http
				.authorizeRequests()
				.anyRequest()
				.permitAll()
				.and()
				.build();
	}
}
