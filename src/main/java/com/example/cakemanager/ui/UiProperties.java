package com.example.cakemanager.ui;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "cakemanager.ui")
@Data
public class UiProperties {
	private String serviceEndpoint;
}
