package com.example.cakemanager.ui.cake;

import com.example.cakemanager.service.cake.model.CreateNewCakeRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.FeignException;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
@RequiredArgsConstructor
class CakeController {
	
	private static final TypeReference<List<Map<String, Object>>> LIST_OF_MAPS_TYPE =
			new TypeReference<>() { };
	
	private final CakeClientService cakeClientService;
	private final ObjectMapper objectMapper;
	private final Optional<OAuth2AuthorizedClientService> authorizedClientService;
	
	@GetMapping
	public ModelAndView index(OAuth2AuthenticationToken auth) {
		return new ModelAndView("index", Map.of(
				"cakes", cakeClientService.findAll(getToken(auth))));
	}

	@GetMapping("/create")
	public ModelAndView create() {
		return new ModelAndView("create");
	}

	@PostMapping("/create")
	public ModelAndView create(String title, String description, String imageUrl,
	                           OAuth2AuthenticationToken auth) {
		return Try.of(() -> cakeClientService.create(getToken(auth), title, description, imageUrl))
				.map(ignored -> index(auth).addObject(
						"successes", List.of(String.format("Cake %s saved", title))))
				.recover(FeignException.UnprocessableEntity.class,
						x -> new ModelAndView("create", Map.of(
								"errors", getErrors(x),
								"createCakeFormModel", new CreateNewCakeRequest(title, description, imageUrl))))
				.get();
	}

	private List<String> getErrors(FeignException.UnprocessableEntity x) {
		return x.responseBody()
				.map(ByteBuffer::array)
				.map(bytes -> Try.of(() -> objectMapper.readValue(bytes, LIST_OF_MAPS_TYPE).stream()
						.map(err -> String.format("%s %s", err.get("field"), err.get("defaultMessage")))
						.collect(Collectors.toList())).get())
				.orElse(Collections.emptyList());
	}

	private String getToken(OAuth2AuthenticationToken auth) {
		return authorizedClientService
				.map(s -> s.loadAuthorizedClient(
						"cakemanager", 
						auth.getPrincipal().getName()))
				.map(c -> ((OAuth2AuthorizedClient) c).getAccessToken().getTokenValue())
				.orElse("");
	}

}
