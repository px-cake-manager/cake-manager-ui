package com.example.cakemanager.ui.cake;

import com.example.cakemanager.service.cake.model.CakeModel;
import com.example.cakemanager.service.cake.model.CreateNewCakeRequest;
import com.example.cakemanager.service.cake.model.ListOfCakesResponse;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

interface CakeClient {
	@RequestLine("GET /cakes")
	@Headers("Authorization: bearer {token}")
	ListOfCakesResponse findAll(@Param("token") String token);

	@RequestLine("POST /cakes")
	@Headers({
			"Content-type: application/json",
			"Accept: application/json", 
			"Authorization: bearer {token}"})
	CakeModel create(@Param("token") String token, CreateNewCakeRequest createNewCakeRequest);
}
