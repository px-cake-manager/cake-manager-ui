package com.example.cakemanager.ui.cake;

import com.example.cakemanager.ui.UiProperties;
import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
class CakeClientConfiguration {
	
	private final UiProperties uiProperties;
	
	@Bean
	public CakeClient cakeClient() {
		return Feign.builder()
				.encoder(new JacksonEncoder())
				.decoder(new JacksonDecoder())
				.target(CakeClient.class, uiProperties.getServiceEndpoint());
	}
}
