package com.example.cakemanager.ui.cake;

import com.example.cakemanager.service.cake.model.CakeModel;
import com.example.cakemanager.service.cake.model.CreateNewCakeRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
class CakeClientService {
	
	private final CakeClient cakeClient;
	
	public List<CakeModel> findAll(String token) {
		return cakeClient.findAll(token).cakes();
	}

	public CakeModel create(String token, String title, String description, String imageUrl) {
		return cakeClient.create(token, new CreateNewCakeRequest(title, description, imageUrl));
	}
}
