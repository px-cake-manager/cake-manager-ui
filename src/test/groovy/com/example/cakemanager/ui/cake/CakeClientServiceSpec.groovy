package com.example.cakemanager.ui.cake

import com.example.cakemanager.service.cake.model.CakeModel
import com.example.cakemanager.service.cake.model.CreateNewCakeRequest
import com.example.cakemanager.service.cake.model.ListOfCakesResponse
import spock.lang.Specification
import spock.lang.Subject

class CakeClientServiceSpec extends Specification {
	
	CakeClient cakeClient = Mock()
	
	@Subject
	CakeClientService cakeClientService = new CakeClientService(cakeClient)
	
	void "Return list of cakes fetched from service"() {
		given:
		List<CakeModel> cakes = Stub()
		ListOfCakesResponse response = new ListOfCakesResponse(cakes)
		cakeClient.findAll("") >> response
		
		when:
		List<CakeModel> r = cakeClientService.findAll("")
		
		then:
		r.is(cakes)
	}
	
	void "Create new cake"() {
		when:
		CakeModel r = cakeClientService.create("", "cake-title", "cake-description", "cake-image-url")
		
		then:
		1 * cakeClient.create(_ as String, _ as CreateNewCakeRequest) >> { String token, CreateNewCakeRequest req ->
			return new CakeModel("some-id", req.title(), req.description(), req.imageUrl())
		}
		
		and:
		r.id() == "some-id"
		r.title() == "cake-title"
		r.description() == "cake-description"
		r.imageUrl() == "cake-image-url"
	}
}
