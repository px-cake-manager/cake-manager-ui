package com.example.cakemanager.ui.cake

import com.example.cakemanager.service.cake.model.CakeModel
import com.example.cakemanager.service.cake.model.CreateNewCakeRequest
import com.fasterxml.jackson.databind.ObjectMapper
import feign.FeignException
import feign.Request
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken
import org.springframework.web.servlet.ModelAndView
import spock.lang.Specification
import spock.lang.Subject

import java.nio.charset.Charset

class CakeControllerSpec extends Specification {
	
	private static final ObjectMapper objectMapper = new ObjectMapper()
	
	CakeClientService cakeClientService = Mock()
	
	@Subject
	CakeController cakeController = new CakeController(cakeClientService, objectMapper, Optional.empty())
	
	void "Index page returns index view with model containing all fetched cakes"() {
		given:
		CakeModel cake1 = new CakeModel("id-1", "cake-1-title", "cake-1-desc", "cake-1-image-url")
		CakeModel cake2 = new CakeModel("id-2", "cake-2-title", "cake-2-desc", "cake-2-image-url")
		cakeClientService.findAll(_ as String) >> [cake1, cake2]
		
		when:
		ModelAndView r = cakeController.index(Stub(OAuth2AuthenticationToken))
		
		then:
		r.viewName == "index"
		
		and:
		with(r.model.cakes) {
			size() == 2
			containsAll([cake1, cake2])
		}
	}
	
	void "Create form"() {
		expect:
		cakeController.create().viewName == "create"
	}
	
	void "Create new cake"() {
		given:
		cakeClientService.findAll(_ as String) >> []
		
		when:
		ModelAndView r = cakeController.create("cake-title", "cake-description", "cake-image-url", Stub(OAuth2AuthenticationToken))
		
		then:
		1 * cakeClientService.create(_, "cake-title", "cake-description", "cake-image-url")
		
		and:
		r.viewName == "index"
		with(r.model.successes) {
			size() == 1
			first() == "Cake cake-title saved"
		}
	}
	
	void "Handle validation errors when creating new cake"() {
		given:
		cakeClientService.create(_ as String, _ as String, _ as String, _ as String) >> {
			throw new FeignException.UnprocessableEntity("422",
					Request.create(
							Request.HttpMethod.POST, 
							"",
							Map.of(), 
							null,
							Charset.defaultCharset()), objectMapper.writeValueAsBytes([
					[code: "ErrorCode1", defaultMessage: "ErrorCode1Msg", field: "field1"],
					[code: "ErrorCode2", defaultMessage: "ErrorCode2Msg", field: "field2"]
			]), null)
		}

		when:
		ModelAndView r = cakeController.create("cake-title", "cake-description", "cake-image-url", Stub(OAuth2AuthenticationToken))
		
		then:
		r.viewName == "create"
		
		and: 
		with((List<String>) r.model.errors) { errs ->
			size() == 2
			errs.contains("field1 ErrorCode1Msg")
			errs.contains("field2 ErrorCode2Msg")
		}
		
		and:
		with( (CreateNewCakeRequest) r.model.createCakeFormModel) { model ->
			model.title() == "cake-title"
			model.description() == "cake-description"
			model.imageUrl() == "cake-image-url"
		}
	}
}
